﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;


public class UnitychanJunp : MonoBehaviour {
    
	void Start () {
		
	}

	void Update () {
        Animator anim = GetComponent<Animator>();
        AnimatorStateInfo state =
            anim.GetCurrentAnimatorStateInfo(0);

        //Jump
        if(Input.GetKey(KeyCode.Space)){
         anim.SetBool("Jump", true);
        }
        if(state.IsName("Locomotion.Jump")){
            anim.SetBool("Jump", false);
        }

        //RunKick
        if(Input.GetKey(KeyCode.V)){
            anim.SetBool("Is_kick",true);
        }
        if(state.IsName("Locomotion.Spinkick")){
            anim.SetBool("Is_kick", false);
        }
           
        //Pose
        if (Input.GetKeyDown(KeyCode.M)){
            anim.SetBool("Pose", true);
        }
        if (state.IsName("Locomotion.Pose")){
            anim.SetBool("Pose", false);
        }
     }
   }