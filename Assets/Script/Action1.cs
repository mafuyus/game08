﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action1 : MonoBehaviour {

    private GameObject obj1;
    private GameObject obj2;
    private GameObject obj3;
    private GameObject obj4;

    // Use this for initialization
	void Start () {
        obj1 = GameObject.Find("Ethan1");
        obj2 = GameObject.Find("Ethan2");
        obj3 = GameObject.Find("Ethan3");
        obj4 = GameObject.Find("mech");
        obj1.SetActive(false);
        obj2.SetActive(false);
        obj3.SetActive(false);
        obj4.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (BoderScript.flag == true){
            obj1.SetActive(true);
            obj2.SetActive(true);
            obj3.SetActive(true);
            obj4.SetActive(true);
        }
	}

    void OnControllerColliderHit(ControllerColliderHit hit){
        CheckItem(hit.collider.gameObject);
    }

    void CheckItem(GameObject obj){
        Animator anim = GetComponent<Animator>();
        if(obj.tag == "Ethan1" ||
           obj.tag == "Ethan2" ||
           obj.tag == "Ethan3"){

            anim.SetBool("is_kick",true);
        }
        AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);

        if(state.IsName("Locomotion.Spinkick")){
            anim.SetBool("is_kick", false);
            obj.GetComponent<Rigidbody>().AddForce(Vector3.forward * 50000,ForceMode.Force);
        }

    }
}
