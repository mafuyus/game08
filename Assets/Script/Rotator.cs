﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

	// Update is called once per frame
	void Update () {

        Transform mytrans = this.gameObject.GetComponent<Transform>();

        Vector3 worldAngle = mytrans.eulerAngles;
        worldAngle.x = 10;
        worldAngle.y = 10;
        worldAngle.z = 10;

        mytrans.Rotate(0,5/2,0,Space.World);
	}
}
